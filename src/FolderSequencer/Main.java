package FolderSequencer;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JFrame;

import FolderSequencer.GUI.RenamerPanel;

/**
 * Lanza el programa
 * 
 * @author David Giordana
 * @version 1.1
 *
 */
public class Main extends JFrame {

	private static final long serialVersionUID = -5307432875341926857L;

	public static void main(String[] args) {
		Main m = new Main();
		m.setVisible(true);
		m.toFront();
		m.requestFocus();
	}
	
	/**
	 * Constructor de la clase
	 */
	public Main(){
		this.getContentPane().setLayout(new BorderLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setMinimumSize(new Dimension(1066,600));
		this.setTitle("File Sequencer 1.1");
		
		RenamerPanel rp = new RenamerPanel();
		this.getContentPane().add(rp);
		
		this.setLocationRelativeTo(null);
	}

	//Agrega un componente a un contenedor
	public static void addComponent(
			Container container, 
			Component component, 
			int gridx, 
			int gridy,
			int gridwidth, 
			int gridheight, 
			double weightx, 
			double weighty, 
			int anchor, 
			int fill) {
		Insets insets = new Insets(0, 3, 0, 3);
		GridBagConstraints gbc = new GridBagConstraints(gridx, gridy, gridwidth, gridheight, weightx, weighty,
				anchor, fill, insets, 0, 0);
		container.add(component, gbc);
	}
	
}
