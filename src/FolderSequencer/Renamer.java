package FolderSequencer;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;

/**
 * Clase con los métodos necesarios para renombrar archivos y carpetas
 * 
 * @author David Giordana
 *
 */
public class Renamer {

	//Valores por defecto
	public static final int DEFAULT_INITIAL_NUMBER = 1;
	public static final int DEFAULT_NUMBER_GAP = 1;

	/**
	 * Renombra una lista de archivos
	 * 
	 * @param folder	 Ruta del diractorio de trabajo
	 * @param fileName Nombre del archivo original
	 * @param separator Separador entre nombre y número
	 * @param initialValue Valor inicial del conteo
	 * @param gap Separación entre números
	 * @param list Lista de archivos a renombrar
	 */
	public void rename(String fileName, String separator, int initialValue, int gap, ArrayList<String> list){
		int digits = getDigitsOf(initialValue + (gap * list.size()));
		for (int i = 0 ; i < list.size() ; i++){
			int number = initialValue + (i * gap);
			rename(list.get(i), fileName, separator, number, digits);
		}
	}

	/**
	 * Renombra un archivo
	 * 
	 * @param filePath Ruta del directorio de trabajo
	 * @param name Nuevo nombre
	 * @param separator Separador entre nombre y número
	 * @param number Número del archivo
	 * @param digits Cantidad de dígitos del número
	 */
	private void rename(String filePath, String name, String separator, int number, int digits){
		String num = String.format ("%0" + digits + "d", number);
		String newName = name + separator + num;
		renameFile(filePath, newName);
	}

	/**
	 * Renombra un archivo
	 * @param path Ruta del directorio a renombrar
	 * @param newName Nuevo nombre
	 */
	public void renameFile(String path, String newName){
		File source = new File(path);

		//Genera el la ruta del directorio padre
		String parentPath = source.getParent();
		if(!parentPath.endsWith(File.separator)){
			parentPath += File.separator;
		}

		//Crea el nuevo nombre
		String extension = FilenameUtils.getExtension(path);
		extension = extension.length() == 0 ? "" : "." + extension;
		File newFile = new File(parentPath + newName + extension);

		//Renombra
		try{
			source.renameTo(newFile);
		}catch (Exception e){
			System.err.print(e.getMessage());
		}
	}

	/**
	 * Toma un entero y retorna la cantidad de Digitos
	 * @param number Numero sobre el cual se contarán los dígitos
	 * @return Número de dígitos
	 */
	private int getDigitsOf(int number){
		return ("" + number).length();
	}

	//Filtro para mostrar solo archivos que no estén ocultos
	public static FilenameFilter onlyVisibleFilesFilter = new FilenameFilter(){
		@Override
		public boolean accept(File dir, String name) {
			return !name.startsWith(".");
		}
	};

}
