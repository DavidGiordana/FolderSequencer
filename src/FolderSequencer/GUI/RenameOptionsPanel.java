package FolderSequencer.GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import FolderSequencer.Main;
import FolderSequencer.Renamer;

/**
 * Panel con opciones de renombre
 * 
 * @author David Giordana
 *
 */
public class RenameOptionsPanel extends JPanel{

	private static final long serialVersionUID = 2409412848366219326L;

	//Campos de entrada
	private JTextField filenameF;
	private JTextField separatorF;

	//Spinners para los números
	private JSpinner initialValue;
	private JSpinner gapValue;

	//Boton para renombrar
	private JButton renameB;
	private JButton clearB;

	//Delegado de la clase
	public RenameOptionsDelegate renameOptionsDelegate;

	/**
	 * Constructor de la clase
	 */
	public RenameOptionsPanel(){
		super(new GridBagLayout());
		this.setBorder(new CompoundBorder(this.getBorder(),  new EmptyBorder(10,10,10,10)));

		//Crea los componentes de la primera linea del panel -> Nombre del archivo
		JLabel label1 = new JLabel("Nombre: ");
		filenameF = new JTextField();

		//Crea los componentes de la sugunda linea del panel -> Separador
		JLabel label2 = new JLabel("Separador. (Por defecto \" \")");
		separatorF = new JTextField();

		//Crea los componentes de la tercera linea del panel -> Valor inicial y gap
		JLabel label3 = new JLabel("Valor inicial");
		JLabel label4 = new JLabel("Salto entre pasos");
		initialValue = new JSpinner(new SpinnerNumberModel(Renamer.DEFAULT_INITIAL_NUMBER, Renamer.DEFAULT_INITIAL_NUMBER, 1000000, 1));
		gapValue = new JSpinner(new SpinnerNumberModel(Renamer.DEFAULT_NUMBER_GAP, Renamer.DEFAULT_NUMBER_GAP, 1000000, 1));

		//Crea los botones
		clearB = new JButton("Limpiar");
		clearB.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(renameOptionsDelegate != null){
					renameOptionsDelegate.clear();
				}
			}

		});
		renameB = new JButton("Renombrar");
		renameB.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				performRename();
			}

		});

		//Agrega los componentes al panel
		Main.addComponent(this, label1				, 0, 0, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
		Main.addComponent(this, filenameF			, 1, 0, 3, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH);
		Main.addComponent(this, clearB				, 4, 0, 1, 1, 0, 1, GridBagConstraints.EAST, GridBagConstraints.BOTH);
		Main.addComponent(this, label2				, 0, 1, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.NONE);
		Main.addComponent(this, separatorF			, 1, 1, 3, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);
		Main.addComponent(this, label3				, 0, 2, 1, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
		Main.addComponent(this, initialValue			, 1, 2, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL);
		Main.addComponent(this, label4				, 2, 2, 1, 1, 0, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL);
		Main.addComponent(this, gapValue				, 3, 2, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL);
		Main.addComponent(this, renameB				, 4, 2, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH);			

	}

	/**
	 * Retorna el saperador de utilizado entre los componentes del nombre
	 * @return Saperador de utilizado entre los componentes del nombre
	 */
	private String getSeparator(){
		String text = separatorF.getText();
		return text == null ? " " : text;
	}

	/**
	 * Intenta aplicar el renombre según los datos del panel de opciones 
	 */
	private void performRename(){
		final String name = filenameF.getText();
		if(name == null || name == ""){
			return;
		}
		if (renameOptionsDelegate != null){
			Thread t = new Thread(new Runnable(){
				@Override
				public void run() {
					renameB.setEnabled(false);
					clearB.setEnabled(false);
					renameOptionsDelegate.rename(name, getSeparator(), (int) initialValue.getValue(), (int) gapValue.getValue());
					renameB.setEnabled(true);
					clearB.setEnabled(true);
				}
			});
			t.start();
		}
	}

	/**
	 * Interfaz del delegado para establecer el comportamiento de los botones del panel
	 * 
	 * @author David Giordana
	 *
	 */
	public interface RenameOptionsDelegate{

		/**
		 * Renombra archivos según los datos brindados por el panel de opciones
		 * @param name Nombre del archivo
		 * @param separator Separador entre componentes del nombre
		 * @param initialValue Valor inicial
		 * @param gapValue Valor de separación entre elementos
		 */
		public void rename(String name, String separator, int initialValue, int gapValue);

		//Aplica la acción de limpieza de datos (No afecta a los campos de este panel)
		public void clear();
	}

}
