package FolderSequencer.GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import FolderSequencer.Main;

/**
 * Panel con las opciones para agregar el contenido de un directorio
 * 
 * @author David Giordana
 *
 */
public class DirectoryAdderPanel extends JPanel{

	private static final long serialVersionUID = 130438024858376528L;

	//Delegado para accionar sobre el directorio actual en caso de ser utilizado
	public DirectoryAdderDelegate directoryAdderDelegate; 

	//Campo de texto del directorio a explorar
	private JTextField directoryField;

	//Botones de acción
	private JButton browse;
	private JButton add;

	/**
	 * Cosntructor
	 */
	public DirectoryAdderPanel(){
		super(new GridBagLayout());
		this.setBorder(new CompoundBorder(this.getBorder(),  new EmptyBorder(10,10,10,10)));

		//Crea la etiqueta previa al campo de texto
		JLabel label = new JLabel("Directiorio:");

		//Crea el resto de componentes del panel
		this.createDirectoryField();
		this.createButtons();

		//Agrega los componentes al panel
		Main.addComponent(this, label, 				0, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE);
		Main.addComponent(this, directoryField, 		1, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL);
		Main.addComponent(this, browse, 				2, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE);
		Main.addComponent(this, add, 				3, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE);
	}

	/**
	 * Crea el campo de texto donde se ingresarán las rutas de directorios
	 */
	private void createDirectoryField(){
		directoryField = new JTextField();
		directoryField.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				performAdd();
			}

		});
	}

	/**
	 * Crea la botonera del panel
	 */
	private void createButtons(){
		//Crea el botón de explorar
		browse = new JButton("Explorar");
		browse.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				performBrowse();
			}

		});

		//Crea el botón de agregar
		add = new JButton("Agregar Contenido");
		add.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				performAdd();
			}

		});
	}

	/**
	 * Explora el sistema de archivos en buqueda de un directorio
	 */
	private void performBrowse(){
		if(directoryAdderDelegate == null){
			return;
		}
		JFileChooser jfc = new JFileChooser();
		jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		jfc.showOpenDialog(this);
		File f = jfc.getSelectedFile();
		directoryField.setText(f.getAbsolutePath());
	}

	/**
	 * Intenta agregar el contenido de un directorio. En caso de poder llama al delagado
	 */
	private void performAdd(){
		final String text = directoryField.getText();
		final File f = new File(text);
		if(!f.exists()){
			JOptionPane.showMessageDialog(this, "No existe el directorio", "Error", JOptionPane.ERROR_MESSAGE, null);
		}
		else if(!f.isDirectory()){
			JOptionPane.showMessageDialog(this, "No es un directorio", "Error", JOptionPane.ERROR_MESSAGE, null);
		}
		else if (text != null && directoryAdderDelegate != null){
			Thread t = new Thread(new Runnable(){

				@Override
				public void run() {
					directoryAdderDelegate.addContentOfDirectory(f);
					directoryField.setText(null);
				}

			});
			t.start();
		}
	}

	/**
	 * Interface para el delegado de la clase
	 * 
	 * @author David Giordana
	 *
	 */
	public interface DirectoryAdderDelegate{

		//Dado un archivo (directorio) hace algo con el
		public void addContentOfDirectory(File f);

	}

}
