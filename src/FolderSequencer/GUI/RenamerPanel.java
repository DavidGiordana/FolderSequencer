package FolderSequencer.GUI;

import java.awt.BorderLayout;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import org.davidgiordana.ExtraClass.GUI.ECList;
import org.davidgiordana.ExtraClass.GUI.ScrollableList;

import FolderSequencer.Renamer;
import FolderSequencer.GUI.DirectoryAdderPanel.DirectoryAdderDelegate;
import FolderSequencer.GUI.RenameOptionsPanel.RenameOptionsDelegate;

/**
 * Panel con el renombrador de archivos
 * 
 * @author David Giordana
 *
 */
public class RenamerPanel extends JPanel implements ECList.AddRemoveButtonsDelegate, ScrollableList.FileDnDDelegate<String>,  DirectoryAdderDelegate, RenameOptionsDelegate{

	private static final long serialVersionUID = 8938441502552424276L;

	//Panel para agregar contenido de un directorio
	private DirectoryAdderPanel dirAddP;

	//Lista para trabajar con los archivos
	private ECList list;

	//Panel con opciones de renombrado
	private RenameOptionsPanel renOptP;


	/**
	 * Constructor de la clase
	 */
	public RenamerPanel(){
		super(new BorderLayout());
		
		//Crea panel superior
		dirAddP = new DirectoryAdderPanel();
		dirAddP.directoryAdderDelegate = this;
		
		//Crea la lista del medio
		list = new ECList(ECList.ORDER_REMOVE_ADD);
		list.setBorder(new CompoundBorder(list.getBorder(),  new EmptyBorder(10,10,10,10)));
		list.addRemoveButtonsDelegate = this;
		list.getList().fileDnDDelegate = this;
		
		//Crea el panel inferior
		renOptP = new RenameOptionsPanel();
		renOptP.renameOptionsDelegate = this;

		//Agrega los componentes al panel
		this.add(dirAddP, BorderLayout.NORTH);
		this.add(list, BorderLayout.CENTER);
		this.add(renOptP, BorderLayout.SOUTH);
	}

	//MARK: RenameOptionsPanel.RenameOptionsDelegate

	@Override
	public void rename(String name, String separator, int initialValue, int gapValue) {
		Renamer ren = new Renamer();
		ArrayList<String> ls = new ArrayList<String>();
		int size = list.getList().getModel().getSize();
		for(int i = 0 ; i < size; i++){
			ls.add(list.getList().getModel().getElementAt(i));
		}
		ren.rename(name, separator, initialValue, gapValue, ls);
		clear();
	}

	@Override
	public void clear() {
		list.getList().getModel().clear();
	}

	//MARK: DirectoryAdderDelegate

	@Override
	public void addContentOfDirectory(File f) {
		for(File file : f.listFiles(Renamer.onlyVisibleFilesFilter)){
			list.getList().getModel().addElement(file.getAbsolutePath());
		}
	}

	//MARK: AddRemoveButtonsDelegate

	@Override
	public String[] performAddElements() {
		return new String[] {};
	}

	@Override
	public void performRemoveElements(List<String> arg0) {}

	//MARK: ScrollableList.FileDnDDelegate<String>
	
	@Override
	public String importDataFrom(File arg0) {
		return arg0.getAbsolutePath();
	}

}
